Dieser einfache Gamelauncher lie�t die Game_Launcher.ini und startet jede Zeile als Befehl.
Startet ihr die EXE mit einem Parameter( zB: Game_Launcher.exe game.ini ) wird die game.ini anstelle der Game_Launcher.ini benutzt.
Sollte der Parameter keiner existierenden Datei entsprechen, wird eine kleine Meldung zur Handhabung ausgegeben.
Ich benutze ihn um Spiele mit XPadder und/oder Windowed Borderless Gaming usw. zu starten.