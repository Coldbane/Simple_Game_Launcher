//============ Copyright Coldbane Software, All rights reserved. =================//
//=== Game_Launcher.cpp : Defines the entry point for the console application. ===//
//================================================================================//
#include <stdafx.h>
#include <windows.h>
#include <iostream>
#include <fstream>
#include <string>
#include <stdlib.h>

int main(int argc, char *argv[])
{
	using namespace std;

	ShowWindow(GetConsoleWindow(), SW_HIDE);			// hides the console window, on default

	string myArg;
	string line;

	if (argc <= 1)										//checks if user made a input
	{
		myArg = "Game_Launcher.ini";					//use default data file if no input was made
	}

	else
	{
		myArg = argv[1];								//if the user made a input, use it instead of default
	}

	ifstream myfile(myArg);

	if (myfile.is_open())								//main function to read the data and run the lines of the datafile
	{
		while (!myfile.eof())
		{
			getline(myfile, line);						//reads the data file
			system(line.c_str());						//runs the lines of the datafile
		}
		myfile.close();
	}

	else
	{
		string myStr1;
		string myStr2;
		string myStr3;
		string myStrNewLine;
		string myStrOutput;

		myStr1 = "Unable to open: ";
		myStr2 = "Usage:";
		myStr3 = "Game_Launcher.exe <Filename>";
		myStrNewLine = "\n";

		myStrOutput = myStr1 + myArg + myStrNewLine + myStr2 + myStrNewLine + myStr3;

		MessageBoxA(NULL, myStrOutput.c_str(), "ERROR! File not found", MB_ICONWARNING | MB_OK);
		return FALSE;
	}

	return 0;
}